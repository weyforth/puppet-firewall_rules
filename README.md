# Puppet Firewall Rules Module

```ruby
mod "puppetlabs/firewall", "1.4.0"

# dep: puppetlabs/firewall
mod "weyforth/firewall_rules",
	git: "https://weyforth@bitbucket.org/weyforth/puppet-firewall_rules.git",
	ref: "master"
```
